package examen;

import java.text.NumberFormat;

/**
 * Clase <code>Account</code> Contiene los atributos y metodos de la clase
 * Account.
 *
 * @author Adrian Puig
 * @version 1.0
 * @see iam.cat
 */
public class Account {

	private NumberFormat fmt = NumberFormat.getCurrencyInstance();

	private final float interesAnual = 0.1f; // interest rate of 10%

	private long acctNumber;
	private float balance;
	public final String name;

	/**
	 * Constructor de la clase Account.
	 * 
	 * @param Nombre   del propietario de la cuenta
	 * @param Numero   de cuenta
	 * @param Cantidad inicial de la cuenta
	 */

	public Account(String owner, long account, float initial) {
		name = owner;
		acctNumber = account;
		balance = initial;
	}

	/**
	 * Metodo de la clase Account que permite ingressar dinero, comprueba si la
	 * cantidad es valida
	 * 
	 * @param Numero de la cantidad a ingresar
	 */
	public boolean ingresar(float amount) {
		boolean result = true;
		// comprobar que la quantitat sigui vàlida
		if (amount < 0) {
			result = false;
		} else {
			balance = balance + amount;
		}

		return result;
	}

	/**
	 * Metodo de la clase Account que permite retirar dinero.
	 * 
	 * @param Numero de la cantidad que se desea retirar
	 * @param Numero de la comision que se cobra por retirar dinero
	 */

	public boolean treureDiners(float amount, float comisio) {
		// validar parameters
		if (isValidWithdrawl(amount, comisio)) {
			amount += comisio;
			balance = balance - amount;
		}
		return isValidWithdrawl(amount, comisio);
	}

	/**
	 * Metodo de la clase Account que permite saber si hay suficiente cantidad de
	 * dinero para retirar-lo
	 * 
	 * @param Numero de la cantidad a retirar
	 * @param Numero de la comision
	 */

	private boolean isValidWithdrawl(float amount, float comisio) {
		// s´ha de comprobar que al compte hi ha suficients diners
		// la suma del amount i la comisió ha de ser més petita que el balance
		if (amount < 0 || comisio < 0 || (amount + comisio) > balance) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Metodo de la clase Account que añade el interes a la cuenta
	 */
	public void afegirInteresAnual() {
		balance += (balance * interesAnual);
	}

	/**
	 * Metodo de la clase Account para hacer la transferencia
	 * 
	 * @param Numero de la cuenta de la persona a la que haces la transferencia
	 * @param Numero que indica la cantidad con la que haces la transferencia
	 */

	public boolean transfer(Account compteDesti, float quantitatTransferir) {

		// comprobar que al balance hi hagi suficients diners i afegir-los al compte
		// destí

		if (quantitatTransferir > balance || compteDesti == null) {

			return false;

		} else {
			balance = balance - quantitatTransferir;

			compteDesti.ingresar(quantitatTransferir);

			return true;
		}
	}

	public float getBalance() {
		return balance;
	}

	public long getAccountNumber() {
		return acctNumber;
	}

	public String toString() {
		return (acctNumber + "\t" + name + "\t" + fmt.format(balance));
	}
}
