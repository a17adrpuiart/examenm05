package examen;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Clase <code>AccountTest</code> Contiene los atributos y metodos de la clase
 * AccountTest.
 *
 * @author Adrian Puig
 * @version 1.0
 * @see iam.cat
 */

class AccountTest {

	Account account;

	
	@BeforeEach
	void setUp() throws Exception {
		account = new Account("ExamenUF5", 1, 100);
	}

	@AfterEach
	void tearDown() throws Exception {
		account = null;
	}

	
	/**
	 * Test de la clase AccountTest que controla la retirada de dinero.
	 */
	@Test
	void testTreureDiners() {
		assertEquals(true, account.treureDiners(50, 5));
		assertEquals(true, account.treureDiners(-5, 50));
		assertEquals(true, account.treureDiners(50, -5));
		
		assertEquals(false, account.treureDiners(50, 5));
		assertEquals(false, account.treureDiners(-5, 50));
		assertEquals(false, account.treureDiners(50, -5));
	} 
	/**
	 * Test de la clase AccountTest que controla el interes anual de la cuenta.
	 */
	@Test
	void testAfegirInteresAnual() {
		account.afegirInteresAnual();
		assertEquals(50, account.getBalance());
	}
	/**
	 * Test de la clase AccountTest que controla el ingreso de dinero.
	 */
	@Test
	void testIngresar() {
		assertEquals(true, account.ingresar(50));
		assertEquals(true, account.ingresar(-5));
		assertEquals(true, account.ingresar(0));


	}

	
}
